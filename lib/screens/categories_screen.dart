import 'package:flutter/material.dart';
import 'package:meals_app/dummy_data.dart';
import 'package:meals_app/widgets/catagory_item.dart';

class CategoriesScreen extends StatelessWidget {
  const CategoriesScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: const EdgeInsets.all(25),
      children: DUMMY_CATEGORIES
          .map((cat) => CategoryItem(cat.id, cat.title, cat.color))
          .toList(),
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200, // Max Width per item
        childAspectRatio:
            3 / 2, // Ratio of Item if width = 200 the height will be 300
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
    );
  }
}
