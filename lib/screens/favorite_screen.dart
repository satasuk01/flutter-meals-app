import 'package:flutter/material.dart';
import 'package:meals_app/models/meal.dart';
import 'package:meals_app/widgets/meal_item.dart';

class FavoritesScreen extends StatelessWidget {
  final List<Meal> favoriteMeals;

  const FavoritesScreen(this.favoriteMeals);

  @override
  Widget build(BuildContext context) {
    if (favoriteMeals.isEmpty) {
      return Container(
        child: null,
      );
    } else {
      return ListView.builder(
        itemCount: favoriteMeals.length,
        itemBuilder: (ctx, i) {
          final displayedMeal = favoriteMeals[i];
          return MealItem(
            id: displayedMeal.id,
            title: displayedMeal.title,
            imageUrl: displayedMeal.imageUrl,
            duration: displayedMeal.duration,
            complexity: displayedMeal.complexity,
            affordability: displayedMeal.affordability,
          );
        },
      );
    }
  }
}
