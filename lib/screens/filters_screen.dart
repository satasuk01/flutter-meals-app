import 'package:flutter/material.dart';
import 'package:meals_app/widgets/main_drawer.dart';

class FiltersScreen extends StatefulWidget {
  static const routeName = '/filters';

  final Function(Map<String, bool>) saveFilters;
  final Map<String, bool> currentFilters;

  FiltersScreen(this.currentFilters, this.saveFilters);

  @override
  _FiltersScreenState createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  bool _glutenFree = true;
  bool _vegetarian = false;
  bool _vegan = false;
  bool _lactoseFree = false;

  // @override
  // void initState() {
  //   _glutenFree = widget.currentFilters['gluten'];
  //   _vegetarian = widget.currentFilters['vegetarian'];
  //   _vegan = widget.currentFilters['vegan'];
  //   _lactoseFree = widget.currentFilters['lactose'];
  //   super.initState();
  // }

  // @override
  // void didChangeDependencies() {
  //   _glutenFree = widget.currentFilters['gluten'];
  //   _vegetarian = widget.currentFilters['vegetarian'];
  //   _vegan = widget.currentFilters['vegan'];
  //   _lactoseFree = widget.currentFilters['lactose'];
  //   super.didChangeDependencies();
  // }

  Widget _buildSwitch(
    String title,
    String subtitle,
    bool value,
    Function updateVal,
  ) {
    return SwitchListTile(
      title: Text(title),
      subtitle: Text(subtitle),
      value: value,
      onChanged: (val) {
        updateVal(val);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Filters'),
          actions: [
            IconButton(
                onPressed: () {
                  final selectedFilter = {
                    'gluten': _glutenFree,
                    '_vegeterian': _vegetarian,
                    '_vegan': _vegan,
                    '_lactoseFree': _lactoseFree,
                  };
                  widget.saveFilters(selectedFilter);
                },
                icon: Icon(Icons.save))
          ],
        ),
        drawer: MainDrawer(),
        body: Column(
          children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Text(
                'Adjust your meal Selection.',
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
            Expanded(
              child: ListView(
                children: [
                  _buildSwitch('Gluten-free', 'Only include gluten-free meals',
                      _glutenFree, (val) {
                    setState(() {
                      _glutenFree = val;
                    });
                  }),
                  _buildSwitch('Lactose-free',
                      'Only include lactose-free meals', _lactoseFree, (val) {
                    setState(() {
                      _lactoseFree = val;
                    });
                  }),
                  _buildSwitch('Vegetarian', 'Only include vegetarian meals',
                      _vegetarian, (val) {
                    setState(() {
                      _vegetarian = val;
                    });
                  }),
                  _buildSwitch('Vegan', 'Only include vegan meals', _vegan,
                      (val) {
                    setState(() {
                      _vegan = val;
                    });
                  }),
                ],
              ),
            )
          ],
        ));
  }
}
