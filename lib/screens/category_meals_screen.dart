import 'dart:math';

import 'package:flutter/material.dart';
// import 'package:meals_app/dummy_data.dart';
import 'package:meals_app/models/meal.dart';
import 'package:meals_app/widgets/meal_item.dart';

class CategoryMealsScreen extends StatefulWidget {
  // final String categoryId;
  // final String categoryTitle;
  final List<Meal> availbleMeals;

  const CategoryMealsScreen(this.availbleMeals);

  static const routeName = '/category-meals';

  @override
  _CategoryMealsScreenState createState() => _CategoryMealsScreenState();
}

class _CategoryMealsScreenState extends State<CategoryMealsScreen> {
  String categoryTitle;
  List<Meal> categoryMeals;
  bool _loadedInitData = false;

  @override
  void didChangeDependencies() {
    // Call after all items are initialiized
    if (!_loadedInitData) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, String>;

      categoryTitle = routeArgs['title'].toString();
      final categoryId = routeArgs['id'].toString();

      // Filter category
      // categoryMeals = DUMMY_MEALS.where((meal) {
      //   return meal.categories.contains(categoryId);
      // }).toList();
      categoryMeals = widget.availbleMeals.where((meal) {
        return meal.categories.contains(categoryId);
      }).toList();
      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  void _removeMeal(String mealId) {
    setState(() {
      categoryMeals.removeWhere((meal) => mealId == meal.id);
    });
  }

  @override
  Widget build(BuildContext context) {
    final availableWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        title: Text('$categoryTitle'),
      ),
      body: Center(
        child: Container(
          width: min(900, availableWidth),
          child: ListView.builder(
            itemCount: categoryMeals.length,
            itemBuilder: (ctx, i) {
              final displayedMeal = categoryMeals[i];
              return MealItem(
                id: displayedMeal.id,
                title: displayedMeal.title,
                imageUrl: displayedMeal.imageUrl,
                duration: displayedMeal.duration,
                complexity: displayedMeal.complexity,
                affordability: displayedMeal.affordability,
                removeItem: _removeMeal,
              );
            },
          ),
        ),
      ),
    );
  }
}
